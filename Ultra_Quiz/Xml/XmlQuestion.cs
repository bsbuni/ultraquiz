﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Ultra_Quiz.Xml
{
    
    [Serializable()]
    [XmlRoot("Quizfrage")]
    public class XmlQuestion      
    {
        private string _questionString;

        [XmlElement("Frage")]
        public string QuestionString
        {
            get { return _questionString; }
            set { _questionString = value; }
        }

        private string _answer;

        [XmlElement("Antwort")]
        public string Answer
        {
            get { return _answer; }
            set { _answer = value; }
        }
        private string _answerRegEx;

        [XmlElement("RegAusdruck")]
        public string AnswerRegEx
        {
            get { return _answerRegEx; }
            set { _answerRegEx = value; }
        }

        private string _level;

        [XmlElement("Level")]
        public string Level
        {
            get { return _level; }
            set { _level = value; }
        }

        private XmlCategory[] _categories;

        [XmlArray("Kategorien")]
        [XmlArrayItem("Kategorie")]
        public XmlCategory[] Categories
        {
            get { return _categories; }
            set { _categories = value; }
        }
    }

    [Serializable()]
    public class XmlCategory
    {
        [XmlAttribute("Name")]
        public string Name { get; set; }
    }
}
