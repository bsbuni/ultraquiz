﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ultra_Quiz.Models;

namespace Ultra_Quiz.Mappings
{
    public class QuestionToCategoryMap : BaseMap<QuestionToCategory>
    {
        public QuestionToCategoryMap()
        {
            Table("QuestionToCategory");

            References(x => x.Category).Column("CategoryId").Not.Nullable().Cascade.None();
            References(x => x.Question).Column("QuestionId").Not.Nullable().Cascade.None();
        }
    }
}
