﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using Ultra_Quiz.Models;

namespace Ultra_Quiz.Mappings
{
    public class BaseMap<T> : ClassMap<T> where T : ModelBase
    {
        public BaseMap()
        {
            Id(x => x.Id).GeneratedBy.Native();
        }
    }
}
