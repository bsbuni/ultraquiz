﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ultra_Quiz.Models;

namespace Ultra_Quiz.Mappings
{
    public class QuestionMap : BaseMap<Question>
    {
        public QuestionMap()
        {
            Table("Question");

            Map(x => x.QuestionString).Column("Question").Not.Nullable().Length(200);
            Map(x => x.Answer).Not.Nullable().Length(50);
            Map(x => x.AnswerRegEx).Nullable().Length(50);
            References(x => x.Level).Column("LevelId").Not.Nullable().NotFound.Ignore().Cascade.All();
            HasMany(x => x.Categories).KeyColumn("QuestionId").Cascade.All().LazyLoad().Inverse();
        }
    }
}
