﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ultra_Quiz.Models;

namespace Ultra_Quiz.Mappings
{
    public class LevelMap : BaseMap<Level>
    {
        public LevelMap()
        {
            Table("Level");

            Map(x => x.Name).Length(20).Not.Nullable();
            Map(x => x.Score);
        }
    }
}
