﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ultra_Quiz.Models;

namespace Ultra_Quiz.Mappings
{
    public class CategoryMap : BaseMap<Category>
    {
        public CategoryMap()
        {
            Table("Category");

            Map(x => x.Name).Length(50).Not.Nullable();
            Map(x => x.Description).Length(200);
            HasMany(x => x.Questions).KeyColumn("CategoryId").Cascade.All().Inverse();
        }
    }
}
