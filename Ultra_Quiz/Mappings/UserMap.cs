﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ultra_Quiz.Models;

namespace Ultra_Quiz.Mappings
{
    public class UserMap : BaseMap<User>
    {
        public UserMap()
        {
            Table("User");

            Map(x => x.Name).Length(50).Not.Nullable();
            Map(x => x.TotalScore);
        }
    }
}
