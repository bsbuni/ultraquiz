﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using FluentNHibernate.Conventions;
using Ultra_Quiz.Configuration;
using Ultra_Quiz.Framework;
using Ultra_Quiz.Models;
using Ultra_Quiz.ViewModels;
using Ultra_Quiz.Views;

namespace Ultra_Quiz.Controllers
{
    internal class WindowEditCategoryController
    {
        private WindowEditCategory _view;
        private WindowEditCategoryViewModel _viewModel;

        public Category NewCategory()
        {
            return EditCategory(new Category());
        }

        public Category EditCategory(Category selectedCategoryModel)
        {
            _view = new WindowEditCategory();
            _viewModel = new WindowEditCategoryViewModel
            {
                Model = selectedCategoryModel,
                OkCommand = new RelayCommand(ExecuteOkCommand),
                CancelCommand = new RelayCommand(ExecuteCancelCommand)
            };

            _view.DataContext = _viewModel;

            return _view.ShowDialog() == true ? _viewModel.Model : null;
        }

        private void ExecuteOkCommand(object obj)
        {
            if (string.IsNullOrEmpty(_viewModel.Name))
            {
                MessageBox.Show("Der Kategorienname muss angegeben werden.");
            }
            else if (_viewModel.Name.Length > 50 || (_viewModel.Description != null && _viewModel.Description.Length > 200))
            {
                MessageBox.Show("Der Name der Kategorie darf maximal 50 Zeichen lang sein. Die Einschränkung für die Beschreibung sind 200 Zeichen.");
            }
            else
            {
                var categoryRepository = new Repository<Category>(QuizConfiguration.DATABASE_PATH);
                var categories = new ObservableCollection<Category>(categoryRepository.GetAll());

                //Get existing Categories with the given Name
                categories = new ObservableCollection<Category>(from category in categories
                    where category.Name.Equals(_viewModel.Name)
                    select category);

                //Check whether a Category with the given Name allready exists
                if (categories.IsNotEmpty())
                {
                    MessageBox.Show("Der Kategoriename ist bereits vorhanden.");
                }
                else
                {
                    _view.DialogResult = true;
                    _view.Close();
                }
            }
        }

        private void ExecuteCancelCommand(object obj)
        {
            _view.DialogResult = false;
            _view.Close();
        }
    }
}