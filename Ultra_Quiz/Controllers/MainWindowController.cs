﻿using System;
using System.Collections.ObjectModel;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Windows;
using Ultra_Quiz.Configuration;
using Ultra_Quiz.Framework;
using Ultra_Quiz.Models;
using Ultra_Quiz.Service;
using Ultra_Quiz.ViewModels;
using Ultra_Quiz.Views;

namespace Ultra_Quiz.Controllers
{
    public class MainWindowController
    {
        //private Repository<Category> _categoryRepository;
        //private Repository<Level> _levelRepository;
        //private Repository<User> _userRepository;
        private MainWindowViewModel _viewModel;
        private IQuizService _quizService;
        private ServiceHost _serviceHost;
        private bool _connected;
        private bool _server;
        private Uri baseAddress = new Uri(QuizConfiguration.SERVICE_HOST);

        public void Initialize()
        {
            _quizService = new QuizService();
            //_levelRepository = new Repository<Level>(QuizConfiguration.DATABASE_PATH);
            //_categoryRepository = new Repository<Category>(QuizConfiguration.DATABASE_PATH);
            //_userRepository = new Repository<User>(QuizConfiguration.DATABASE_PATH);
            var _view = new MainWindow();
            _viewModel = new MainWindowViewModel
            {
                //Levels = new ObservableCollection<Level>(_levelRepository.GetAll()),
                //Categories = new ObservableCollection<Category>(_categoryRepository.GetAll()),
                //Users = new ObservableCollection<User>(_userRepository.GetAll()),

                //No catch for Server-Timeout before MainWindow is initialized
                Levels = _quizService.GetLevels().Levels,
                Categories = _quizService.GetCateories().Categories,
                Users = _quizService.GetUsers().Users,

                SwitchConnectText = "Zu Spielserver verbinden",
                SwitchHostText = "Spielserver starten",

                SwitchHostCommand = new RelayCommand(ExecuteSwitchHost),
                SwitchConnectCommand = new RelayCommand(ExecuteSwitchConnect),

                AdministrationCommand = new RelayCommand(ExecuteAdministrationCommand),
                PlayCommand = new RelayCommand(ExecutePlayCommand),
                UpdateCommand = new RelayCommand(ExecuteUpdateCommand),
                HighscoreCommand = new RelayCommand(ExecuteHighscoreCommand)
            };

            _view.DataContext = _viewModel;

            _view.ShowDialog();
        }

        private void ExecuteAdministrationCommand(object obj)
        {
            if (_server || _connected)
            {
                MessageBox.Show(
                    "Verwaltung kann nicht geöffnet werden, solange Verbindung zu Server besteht oder Anwendung selbst als Server läuft.");
            }
            else
            {
                new AdministrationWindowController().Administrate();
                ExecuteUpdateCommand(obj);
            }
        }

        private void ExecutePlayCommand(object obj)
        {
            //Check whether connection timed out or has been refused
            if (!new PlayWindowController().Play(_quizService,_viewModel.Category, _viewModel.Level, _viewModel.User))
            {
                //Get local data if connection got lost during play view
                ExecuteSwitchConnect(obj);
            }
        }

        //Update
        private void ExecuteUpdateCommand(object obj)
        {
            _viewModel.Level = null;
            _viewModel.Category = null;
            _viewModel.User = null;

            try
            {
                _viewModel.Levels = _quizService.GetLevels().Levels;
                _viewModel.Categories = _quizService.GetCateories().Categories;
                _viewModel.Users = _quizService.GetUsers().Users;
            }
            catch (Exception)
            {
                MessageBox.Show(
                "Serververbindung konnte nicht hergestellt werden.");
                //Connection timeout -> Stop connection
                ExecuteSwitchConnect(obj);
            }

        }

        //Open Highscore
        private void ExecuteHighscoreCommand(object obj)
        {
            //Check whether connection timed out or has been refused
            if (!new HighscoreWindowController().ShowHighscore(_quizService))
            {
                //Get local data if connection got lost during highscore view
                ExecuteSwitchConnect(obj);
            }
        }

        //Switch connection between on and oof
        private void ExecuteSwitchConnect(object obj)
        {
            //Check whether this client instance has a server running in it
            if (_server)
            {
                MessageBox.Show(
                    "Verbindung zu Spieleserver kann nicht hergestellt werden, solange auf diesem Client selbst ein Server läuft.");
            }
            else
            {
                //Check whether this client is connected to a server
                if (_connected)
                {
                    //Disconnect from server
                    _quizService = new QuizService();
                    _connected = false;
                    _viewModel.SwitchConnectText = "Zu Spielserver verbinden";
                    
                    MessageBox.Show(
                        "Die Verbindung wurde getrennt. Das Laden der lokalen Daten kann einen Moment daueren.");
                    ExecuteUpdateCommand(obj);
                    MessageBox.Show("Laden der Daten abgeschlossen.");
                }
                else
                {
                    //Connect to server on given ip address
                    IQuizService quizService = new ServerConnectController().Connect();

                    if (quizService != null)
                    {
                        //Connection successful
                        _quizService = quizService;
                        _connected = true;
                        _viewModel.SwitchConnectText = "Von Spieleserver abmelden";

                        MessageBox.Show(
                            "Verbindung wird hergestellt. Das Laden der Daten vom Server kann einen Moment dauern.");
                        ExecuteUpdateCommand(obj);
                        MessageBox.Show("Laden der Daten abgeschlossen.");
                    }
                }
            }
        }

        //Switch hosting between on and oof
        private void ExecuteSwitchHost(object obj)
        {
            //Check whether this client instance is connected to a server
            if (_connected)
            {
                MessageBox.Show(
                    "Es konnte kein Spielserver gestartet werden, da die Anwendung noch mit einem anderen Spielserver verbunden ist.");
            }
            else
            {
                //Check whether this client is hosting a server
                if (_server)
                {
                    //Close Server
                    _serviceHost.Close();
                    _server = false;
                    _viewModel.SwitchHostText = "Spielserver starten";

                    MessageBox.Show("Server erfolgreich gestoppt.");
                }
                else
                {
                    //Set session timeout
                    var binding = new WSHttpBinding
                    {
                        //Set session timeout
                        ReceiveTimeout = new TimeSpan(0, QuizConfiguration.SESSION_TIMEOUT, 0),
                    };
                    
                    _serviceHost = new ServiceHost(typeof(QuizService));
                    _serviceHost.AddServiceEndpoint("Ultra_Quiz.Service.IQuizService", binding, baseAddress);
                    
                    //Enable metadata publishing
                    var smb = new ServiceMetadataBehavior
                    {
                        HttpGetEnabled = false,
                        MetadataExporter = {PolicyVersion = PolicyVersion.Policy15}
                    };
                    _serviceHost.Description.Behaviors.Add(smb);

                    try
                    {
                        _serviceHost.Open();
                        _server = true;
                        _viewModel.SwitchHostText = "Spieleserver stoppen";
                        
                        MessageBox.Show("Server erfolgreich gestartet.");
                    }
                    catch (Exception)
                    {
                        MessageBox.Show(
                            "Fehler beim Starten des Servers. Starten sie die Anwendung ggf. als Administrator erneut und stellen sie sicher, dass kein anderer Server auf diesem Rechner gestartet ist.");
                    }
                }
            }
        }
    }
}