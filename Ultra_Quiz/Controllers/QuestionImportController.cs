﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Xml.Serialization;
using FluentNHibernate.Conventions;
using Microsoft.Win32;
using Ultra_Quiz.Configuration;
using Ultra_Quiz.Framework;
using Ultra_Quiz.Models;
using Ultra_Quiz.ViewModels;
using Ultra_Quiz.Views;
using Ultra_Quiz.Xml;

namespace Ultra_Quiz.Controllers
{
    public class QuestionImportController
    {
        private ObservableCollection<Category> _categories;
        private Repository<Category> _categoryRepository;
        private Repository<Level> _levelRepository;
        private ObservableCollection<Level> _levels;
        private Repository<Question> _questionRepository;
        private ObservableCollection<Question> _questions;
        private QuestionImport _view;
        private QuestionImportViewModel _viewModel;

        public bool Import()
        {
            _categoryRepository = new Repository<Category>(QuizConfiguration.DATABASE_PATH);
            _categories = new ObservableCollection<Category>(_categoryRepository.GetAll());
            _levelRepository = new Repository<Level>(QuizConfiguration.DATABASE_PATH);
            _levels = new ObservableCollection<Level>(_levelRepository.GetAll());
            _questionRepository = new Repository<Question>(QuizConfiguration.DATABASE_PATH);
            _questions = new ObservableCollection<Question>(_questionRepository.GetAll());

            _view = new QuestionImport();
            _viewModel = new QuestionImportViewModel
            {
                OkCommand = new RelayCommand(ExecuteOkCommand),
                CancelCommand = new RelayCommand(ExecuteCancelCommand),
                BrowseCommand = new RelayCommand(ExecuteBrowseCommand)
            };

            _view.DataContext = _viewModel;


            bool? result = _view.ShowDialog();
            return result != null && result == true;
        }

        private void ExecuteOkCommand(object obj)
        {
            string path = _viewModel.Path;

            var serializer = new XmlSerializer(typeof (XmlQuestion));

            try
            {
                XmlQuestion xmlQuestion;
                using (var stream = new FileStream(path, FileMode.Open))
                {
                    xmlQuestion = serializer.Deserialize(stream) as XmlQuestion;
                }

                if (xmlQuestion != null)
                {
                    //Check whether Question and Answer are set
                    if (!string.IsNullOrEmpty(xmlQuestion.QuestionString) && !string.IsNullOrEmpty(xmlQuestion.Answer) &&
                        xmlQuestion.QuestionString.Length <= 200 && xmlQuestion.Answer.Length <= 50 &&
                        xmlQuestion.AnswerRegEx.Length <= 50)
                    {
                        //Find Questions with the same QuestionString
                        IEnumerable<Question> existingQuestions = from question in _questions
                            where question.QuestionString.Equals(xmlQuestion.QuestionString)
                            select question;

                        //Check whether Questions with the same QuestionString exist
                        if (existingQuestions.IsEmpty())
                        {
                            //Check whether Level is set
                            if (!string.IsNullOrEmpty(xmlQuestion.Level))
                            {
                                //Get persisted Level with the given name
                                Level linkedLevel = (from level in _levels
                                    where level.Name.Equals(xmlQuestion.Level)
                                    select level).FirstOrDefault();

                                //Check whether level with the given name exists
                                if (linkedLevel != null)
                                {
                                    var categoryNames =
                                        new List<string>(
                                            xmlQuestion.Categories.Where(x => !string.IsNullOrEmpty(x.Name))
                                                .Select(x => x.Name));

                                    //Check whether one or more Category are given
                                    if (categoryNames.Count != 0)
                                    {
                                        var linkedCategories =
                                            new ObservableCollection<Category>(from category in _categories
                                                where categoryNames.Contains(category.Name)
                                                select category);

                                        //Check whether the linked Categories exist
                                        if (!linkedCategories.IsEmpty())
                                        {
                                            //Create question with all data
                                            var question = new Question
                                            {
                                                Answer = xmlQuestion.Answer,
                                                AnswerRegEx = xmlQuestion.AnswerRegEx,
                                                QuestionString = xmlQuestion.QuestionString,
                                                Level = linkedLevel
                                            };
                                            _questionRepository.Save(question);

                                            //Add Category-Links
                                            var linkedQuestionToCategories =
                                                new ObservableCollection<QuestionToCategory>(
                                                    linkedCategories.Select(
                                                        x => new QuestionToCategory {Category = x, Question = question}));
                                            question.Categories = linkedQuestionToCategories;
                                            _questionRepository.Save(question);

                                            _view.DialogResult = true;
                                            _view.Close();
                                        }
                                        else
                                        {
                                            MessageBox.Show(
                                                "Mindestens eine der angegebenen Kategorien muss existieren.");
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("Es muss mindestens eine Kategorie angegeben werden.");
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Der angegebene Schwierigkeitsgrad muss existieren");
                                }
                            }
                            else
                            {
                                MessageBox.Show("Es muss ein Schwierigkeitsgrad angegeben werden.");
                            }
                        }
                        else
                        {
                            MessageBox.Show("Die Frage existiert bereits.");
                        }
                    }
                    else
                    {
                        MessageBox.Show(
                            "Frage und Antwort müssen angegeben sein. Die Frage darf nicht länger als 200 Zeichen sein. Die Einschränkung für die Antwort und den regulären Ausdruck sind 50 Zeichnen");
                    }
                }
                else
                {
                    MessageBox.Show("Fehler beim Import.");
                }
            }
            catch (Exception)
            {
                MessageBox.Show(
                    "Der angegebene Pfad beinhaltet keine Datei des vorgegebenen Formats. Überprüfen sie die Datei oder starten sie die Anwendung ggf. als Administrator erneut.");
            }
        }

        private void ExecuteCancelCommand(object obj)
        {
            _view.DialogResult = false;
            _view.Close();
        }

        private void ExecuteBrowseCommand(object obj)
        {
            //Open a OpenFileDialog to browse for Files
            var ofd = new OpenFileDialog {DefaultExt = ".xml"};

            bool? result = ofd.ShowDialog();
            if (result != null && result == true)
            {
                _viewModel.Path = ofd.FileName;
            }
            else
            {
                MessageBox.Show("Fehlerhafte Auswahl.");
            }
        }
    }
}