﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using NHibernate;
using NHibernate.Linq;
using Ultra_Quiz.Configuration;
using Ultra_Quiz.Framework;
using Ultra_Quiz.Models;
using Ultra_Quiz.ViewModels;
using Ultra_Quiz.Views;

namespace Ultra_Quiz.Controllers
{
    public class AdministrationWindowController
    {
        private Repository<Category> _categoryRepository;
        private Repository<Level> _levelRepository;
        private Repository<Question> _questionRepository;
        private Repository<User> _userRepository;
        private AdministrationWindowViewModel _viewModel;
        private AdministrationWindow _view;

        public void Administrate()
        {
            _questionRepository = new Repository<Question>(QuizConfiguration.DATABASE_PATH);
            _levelRepository = new Repository<Level>(QuizConfiguration.DATABASE_PATH);
            _categoryRepository = new Repository<Category>(QuizConfiguration.DATABASE_PATH);
            _userRepository = new Repository<User>(QuizConfiguration.DATABASE_PATH);

            _view = new AdministrationWindow();
            _viewModel = new AdministrationWindowViewModel
            {
                QuestionModels = new ObservableCollection<Question>(_questionRepository.GetAll()),
                LevelModels = new ObservableCollection<Level>(_levelRepository.GetAll()),
                CategoryModels = new ObservableCollection<Category>(_categoryRepository.GetAll()),
                UserModels = new ObservableCollection<User>(_userRepository.GetAll()),

                UpdateCommand = new RelayCommand(UpdateCommandExecute),
                CloseCommand = new RelayCommand(CloseCommandExecute),

                //Question commands
                NewQuestionCommand = new RelayCommand(NewQuestionCommandExecute),
                EditQuestionCommand = new RelayCommand(EditQuestionCommandExecute, EditQuestionCommandCanExecute),
                DeleteQuestionCommand = new RelayCommand(DeleteQuestionCommandExecute, EditQuestionCommandCanExecute),
                ImportQuestionCommand = new RelayCommand(ImportQuestionCommandExecute),

                //Level commands
                NewLevelCommand = new RelayCommand(NewLevelCommandExecute),
                EditLevelCommand = new RelayCommand(EditLevelCommandExecute, EditLevelCommandCanExecute),
                DeleteLevelCommand = new RelayCommand(DeleteLevelCommandExecute, EditLevelCommandCanExecute),

                //Category commands
                NewCategoryCommand = new RelayCommand(NewCategoryCommandExecute),
                EditCategoryCommand = new RelayCommand(EditCategoryCommandExecute, EditCategoryCommandCanExecute),
                DeleteCategoryCommand = new RelayCommand(DeleteCategoryCommandExecute, EditCategoryCommandCanExecute),

                //User commands
                NewUserCommand = new RelayCommand(NewUserCommandExecute),
                EditUserCommand = new RelayCommand(EditUserCommandExecute, EditUserCommandCanExecute),
                DeleteUserCommand = new RelayCommand(DeleteUserCommandExecute, EditUserCommandCanExecute)
            };

            _view.DataContext = _viewModel;

            _view.ShowDialog();
        }

        private void UpdateCommandExecute(object obj)
        {
            _questionRepository = new Repository<Question>(QuizConfiguration.DATABASE_PATH);
            _levelRepository = new Repository<Level>(QuizConfiguration.DATABASE_PATH);
            _categoryRepository = new Repository<Category>(QuizConfiguration.DATABASE_PATH);
            _userRepository = new Repository<User>(QuizConfiguration.DATABASE_PATH);

            _viewModel.QuestionModels = new ObservableCollection<Question>(_questionRepository.GetAll());
            _viewModel.LevelModels = new ObservableCollection<Level>(_levelRepository.GetAll());
            _viewModel.CategoryModels = new ObservableCollection<Category>(_categoryRepository.GetAll());
            _viewModel.UserModels = new ObservableCollection<User>(_userRepository.GetAll());
        }

        private void CloseCommandExecute(object obj)
        {
            _view.Close();
        }

        //Question command executions---------------------------------------------------------------------------------------------------------
        private void NewQuestionCommandExecute(object obj)
        {
            if (new WindowEditQuestionController().NewQuestion())
            {
                _viewModel.QuestionModels = new ObservableCollection<Question>(_questionRepository.GetAll());
            }
        }

        private void EditQuestionCommandExecute(object obj)
        {
            if (_viewModel.SelectedQuestionModel != null)
            {
                if (new WindowEditQuestionController().EditQuestion(_viewModel.SelectedQuestionModel))
                {
                    _viewModel.QuestionModels = new ObservableCollection<Question>(_questionRepository.GetAll());
                }
            }
        }

        private void DeleteQuestionCommandExecute(object obj)
        {
            //Deletion not possible because of NHibernate resaving it from connections to categories
            MessageBox.Show("Löschen von Fragen ist nicht untersützt, aufgrund von Problemen mit NHibernate.");
            /*if (_viewModel.SelectedQuestionModel != null)
            {
                _questionRepository.Delete(_viewModel.SelectedQuestionModel);
                _viewModel.QuestionModels.Remove(_viewModel.SelectedQuestionModel);
            }*/
        }

        private bool EditQuestionCommandCanExecute(object obj)
        {
            return _viewModel.SelectedQuestionModel != null;
        }

        private void ImportQuestionCommandExecute(object obj)
        {
            if (new QuestionImportController().Import())
            {
                MessageBox.Show("Frage wurde erfolgreich importiert");
                _viewModel.QuestionModels = new ObservableCollection<Question>(_questionRepository.GetAll());
            }
        }

        //Level command executions---------------------------------------------------------------------------------------------------------
        private void NewLevelCommandExecute(object obj)
        {
            Level addedObject = new WindowEditLevelController().NewLevel();
            if (addedObject != null)
            {
                _viewModel.LevelModels.Add(addedObject);
                _levelRepository.Save(addedObject);
            }
        }

        private void EditLevelCommandExecute(object obj)
        {
            if (_viewModel.SelectedLevelModel != null)
            {
                Level editedObject = new WindowEditLevelController().EditLevel(_viewModel.SelectedLevelModel);
                if (editedObject != null)
                {
                    _levelRepository.Save(editedObject);
                    _viewModel.LevelModels = new ObservableCollection<Level>(_levelRepository.GetAll());
                }
            }
        }

        private void DeleteLevelCommandExecute(object obj)
        {
            //Deletion not possible because of NHibernate resaving it from the underlying connections of the connected questions to categories
            MessageBox.Show("Löschen von Schwierigkeitsgrade ist nicht unterstützt, aufgrund von Problemen mit NHibernate.");
         
            /*if (
                MessageBox.Show(
                    "Alle Fragen, die diesem Schwierigkeitsgrad zugeordnet sind, werden gelöscht. Trotzdem fortfahren?",
                    "Fortfahren?", MessageBoxButton.YesNo) == MessageBoxResult.Yes &&
                _viewModel.SelectedLevelModel != null)
            {
               var questionToCategoryRepository = new Repository<QuestionToCategory>(QuizConfiguration.DATABASE_PATH);

                var questions =
                    new List<Question>(from question in new ObservableCollection<Question>(_questionRepository.GetAll())
                        where question.Level.Id == _viewModel.SelectedLevelModel.Id
                        select question);

                using (var session = NHibernateHelper.OpenSession())
                {
                    //Delete alle questions that are linked to this Level
                    var questionToCategories = new List<QuestionToCategory>();
                    questions.ForEach(x =>
                    {
                        var anything = session.Get<Question>(x.Id);
                        NHibernateUtil.Initialize(anything.Categories);
                        questionToCategories.AddRange(anything.Categories);
                    });
                    questionToCategories.ForEach(questionToCategoryRepository.Delete);

                    questions.ForEach(_questionRepository.Delete);

                    _levelRepository.Delete(_viewModel.SelectedLevelModel);
                    _viewModel.LevelModels.Remove(_viewModel.SelectedLevelModel);

                }
            }*/
        }

        private bool EditLevelCommandCanExecute(object obj)
        {
            return _viewModel.SelectedLevelModel != null;
        }

        //Category command executions---------------------------------------------------------------------------------------------------------
        private void NewCategoryCommandExecute(object obj)
        {
            Category addedObject = new WindowEditCategoryController().NewCategory();
            if (addedObject != null)
            {
                _viewModel.CategoryModels.Add(addedObject);
                _categoryRepository.Save(addedObject);
            }
        }

        private void EditCategoryCommandExecute(object obj)
        {
            if (_viewModel.SelectedCategoryModel != null)
            {
                Category editedObject = new WindowEditCategoryController().EditCategory(_viewModel.SelectedCategoryModel);
                if (editedObject != null)
                {
                    _categoryRepository.Save(editedObject);
                    _viewModel.CategoryModels = new ObservableCollection<Category>(_categoryRepository.GetAll());
                }
            }
        }

        private void DeleteCategoryCommandExecute(object obj)
        {
            //Deletion not possible because of NHibernate resaving it from connections to questions
            MessageBox.Show("Löschen von Kategorien ist nicht untersützt, aufgrund von Problemen mit NHibernate.");

            /*if (
                MessageBox.Show("Alle Fragen, dieser Kategorie zugeordnet sind, werden gelöscht. Trotzdem fortfahren?",
                    "Fortfahren?", MessageBoxButton.YesNo) == MessageBoxResult.Yes &&
                _viewModel.SelectedCategoryModel != null)
            {
                var questionToCategoryRepository = new Repository<QuestionToCategory>(QuizConfiguration.DATABASE_PATH);

                //Delete alle questions that are linked to this Category
                (from questionToCategory in
                    new ObservableCollection<QuestionToCategory>(questionToCategoryRepository.GetAll())
                    where questionToCategory.Category.Id == _viewModel.SelectedCategoryModel.Id
                    select questionToCategory).ForEach(x =>
                    {
                        Question question = x.Question;
                        questionToCategoryRepository.Delete(x);
                        _questionRepository.Delete(question);
                    });

                _categoryRepository.Delete(_viewModel.SelectedCategoryModel);
                _viewModel.CategoryModels.Remove(_viewModel.SelectedCategoryModel);
            }*/
        }

        private bool EditCategoryCommandCanExecute(object obj)
        {
            return _viewModel.SelectedCategoryModel != null;
        }

        //User command executions---------------------------------------------------------------------------------------------------------
        private void NewUserCommandExecute(object obj)
        {
            User addedObject = new WindowEditUserController().NewUser();
            if (addedObject != null)
            {
                _viewModel.UserModels.Add(addedObject);
                _userRepository.Save(addedObject);
            }
        }

        private void EditUserCommandExecute(object obj)
        {
            if (_viewModel.SelectedUserModel != null)
            {
                User editedObject = new WindowEditUserController().EditUser(_viewModel.SelectedUserModel);
                if (editedObject != null)
                {
                    _userRepository.Save(editedObject);
                    _viewModel.UserModels = new ObservableCollection<User>(_userRepository.GetAll());
                }
            }
        }

        private void DeleteUserCommandExecute(object obj)
        {
            if (_viewModel.SelectedUserModel != null)
            {
                _userRepository.Delete(_viewModel.SelectedUserModel);
                _viewModel.UserModels.Remove(_viewModel.SelectedUserModel);
            }
        }

        private bool EditUserCommandCanExecute(object obj)
        {
            return _viewModel.SelectedUserModel != null;
        }
    }
}