﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using FluentNHibernate.Conventions;
using Ultra_Quiz.Configuration;
using Ultra_Quiz.Framework;
using Ultra_Quiz.Models;
using Ultra_Quiz.ViewModels;
using Ultra_Quiz.Views;

namespace Ultra_Quiz.Controllers
{
    public class WindowEditLevelController
    {
        private WindowEditLevel _view;
        private WindowEditLevelViewModel _viewModel;

        public Level NewLevel()
        {
            return EditLevel(new Level());
        }

        public Level EditLevel(Level selectedLevelModel)
        {
            _view = new WindowEditLevel();
            _viewModel = new WindowEditLevelViewModel
            {
                Model = selectedLevelModel,
                OkCommand = new RelayCommand(ExecuteOkCommand),
                CancelCommand = new RelayCommand(ExecuteCancelCommand)
            };

            _view.DataContext = _viewModel;

            return _view.ShowDialog() == true ? _viewModel.Model : null;
        }

        private void ExecuteOkCommand(object obj)
        {
            if (string.IsNullOrEmpty(_viewModel.Name) || _viewModel.Score == 0)
            {
                MessageBox.Show("Der Levelname sowie eine Punktzahl größer 0 muss angegeben werden.");
            }
            else if (_viewModel.Name.Length > 20)
            {
                MessageBox.Show("Der Levelname darf maximal 20 Zeichen lang sein.");
            }
            else
            {
                var levelRepository = new Repository<Level>(QuizConfiguration.DATABASE_PATH);
                var levels = new ObservableCollection<Level>(levelRepository.GetAll());

                //Get existing Levels with the given Name
                levels = new ObservableCollection<Level>(from level in levels
                    where level.Name.Equals(_viewModel.Name)
                    select level);

                //Check whether a Level with the given Name allready exist
                if (levels.IsNotEmpty())
                {
                    MessageBox.Show("Der Levelname ist bereits vorhanden.");
                }
                else
                {
                    _view.DialogResult = true;
                    _view.Close();
                }
            }
        }

        private void ExecuteCancelCommand(object obj)
        {
            _view.DialogResult = false;
            _view.Close();
        }
    }
}