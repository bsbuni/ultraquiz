﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using FluentNHibernate.Conventions;
using Ultra_Quiz.Configuration;
using Ultra_Quiz.Framework;
using Ultra_Quiz.Models;
using Ultra_Quiz.ViewModels;
using Ultra_Quiz.Views;

namespace Ultra_Quiz.Controllers
{
    internal class WindowEditUserController
    {
        private WindowEditUser _view;
        private WindowEditUserViewModel _viewModel;

        public User NewUser()
        {
            return EditUser(new User());
        }

        public User EditUser(User selectedUserModel)
        {
            _view = new WindowEditUser();
            _viewModel = new WindowEditUserViewModel
            {
                Model = selectedUserModel,
                OkCommand = new RelayCommand(ExecuteOkCommand),
                CancelCommand = new RelayCommand(ExecuteCancelCommand)
            };

            _view.DataContext = _viewModel;

            return _view.ShowDialog() == true ? _viewModel.Model : null;
        }

        private void ExecuteOkCommand(object obj)
        {
            if (string.IsNullOrEmpty(_viewModel.Name))
            {
                MessageBox.Show("Der Nutzername muss eingegeben werden.");
            }
            else if (_viewModel.Name.Length > 50)
            {
                MessageBox.Show("Der Nutzername darf maximal 50 Zeichen lang sein.");    
            }
            else
            {
                var userRepository = new Repository<User>(QuizConfiguration.DATABASE_PATH);
                var users = new ObservableCollection<User>(userRepository.GetAll());

                //Get existing Users with the given name
                users = new ObservableCollection<User>(from user in users
                    where user.Name.Equals(_viewModel.Name)
                    select user);

                //Check whether a User with the given name allready exists
                if (users.IsNotEmpty())
                {
                    MessageBox.Show("Der Nutzername ist bereits vorhanden.");
                }
                else
                {
                    _view.DialogResult = true;
                    _view.Close();
                }
            }
        }

        private void ExecuteCancelCommand(object obj)
        {
            _view.DialogResult = false;
            _view.Close();
        }
    }
}