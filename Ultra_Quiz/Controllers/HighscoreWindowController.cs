﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using Ultra_Quiz.Configuration;
using Ultra_Quiz.Framework;
using Ultra_Quiz.Models;
using Ultra_Quiz.Service;
using Ultra_Quiz.ViewModels;
using Ultra_Quiz.Views;

namespace Ultra_Quiz.Controllers
{
    public class HighscoreWindowController
    {
        private HighscoreWindow _view;
        private HighscoreWindowViewModel _viewModel;

        public bool ShowHighscore(IQuizService quizService)
        {
            try
            {
                var users = quizService.GetHighscore().Users;
                _view = new HighscoreWindow();
                _viewModel = new HighscoreWindowViewModel
                {
                    Users = users,
                    CloseCommand = new RelayCommand(CloseCommandExecute)
                };

                _view.DataContext = _viewModel;

                _view.ShowDialog();
            }
            catch (Exception)
            {
                MessageBox.Show("Verbindungsfehler. Dieser kann auftreten, wenn sie sich länger als 15 Minuten nicht beim Server gemeldet haben oder die Verbindung zum Server abgebrochen ist.");
                _view.DialogResult = false;
                _view.Close();
            }

            return _view.DialogResult == true ? true : false;
        }

        private void CloseCommandExecute(object obj)
        {
            _view.DialogResult = true;
            _view.Close();
        }
    }
}