﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Threading;
using Ultra_Quiz.Configuration;
using Ultra_Quiz.Framework;
using Ultra_Quiz.Service;
using Ultra_Quiz.ViewModels;
using Ultra_Quiz.Views;
using MessageBox = System.Windows.MessageBox;

namespace Ultra_Quiz.Controllers
{
    public class PlayWindowController
    {
        private PlayWindowViewModel _viewModel;
        private PlayWindow _view;
        private IQuizService _quizService;
        private DispatcherTimer timer;

        public bool Play(IQuizService quizService, CategorySubmit category, LevelSubmit level, UserSubmit user)
        {
            _quizService = quizService;

            if (user != null)
            {
                try
                {
                    if (_quizService.SetUser(user))
                    {
                        //Login successfull
                        ObservableCollection<QuestionSubmit> quizQuestions =
                            _quizService.GetQuestions(category == null ? 0 : category.Id, level == null ? 0 : level.Id)
                                .Questions;

                        if (quizQuestions != null)
                        {
                            QuestionSubmit question = _quizService.GetQuestion(0);
                            if (question != null)
                            {
                                _view = new PlayWindow();
                                _viewModel = new PlayWindowViewModel
                                {
                                    Questions = quizQuestions,
                                    Question = question,
                                    Score = 0,
                                    Correct = 0,
                                    User = user,
                                    Count = 0,
                                    CountDown = QuizConfiguration.COUNTDOWN,
                                    CountDownShown = QuizConfiguration.COUNTDOWN/1000,

                                    AnswerCommand = new RelayCommand(AnswerCommandExecute),
                                    CancelCommand = new RelayCommand(CancelCommandExecute)
                                };

                                _view.DataContext = _viewModel;

                                InitializeCountDown();

                                _view.ShowDialog();
                            }
                            else
                            {
                                MessageBox.Show("Fehler beim Laden der Fragen.");
                                return true;
                            }
                        }
                        else
                        {
                            MessageBox.Show(
                                "Es sind nicht genügend Fragen für das Quiz vorhanden. Ändern sie Einschränkungen von Kategorie/Level oder fügen sie mehr Fragen hinzu.");
                            return true;
                        }
                    }
                    else
                    {
                        //Login not successfull
                        MessageBox.Show(
                            "Login nicht erfolgreich. Eventuell ist der angegebene Nutzer bereits eingeloggt. " +
                            "Sind sie in einer anderen Anwendung als dieser User angemeldet, beenden sie in dieser ggf. die Verbindung und verbinden sie sich erneut");
                        return true;
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Verbindungsfehler. Dieser kann auftreten, wenn sie sich länger als 15 Minuten nicht beim Server gemeldet haben oder die Verbindung zum Server abgebrochen ist.");
                    if (_view != null)
                    {
                        _view.DialogResult = false;
                        _view.Close();
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                MessageBox.Show(
                    "Es muss ein User ausgewählt werden.");
                return true;
            }

            _view.Close();
            return _view.DialogResult != null && (bool)_view.DialogResult;
        }

        //Answer question
        private void AnswerCommandExecute(object obj)
        {
            timer.Stop();
            try
            {
                //Check answer
                var valid = _quizService.CheckQuestion(_viewModel.CountDown, _viewModel.Count, _viewModel.Answer);
           
                if (valid!=null)
                {
                    //Not cheated
                    if (valid.Correct == null)
                    {
                        //Not Answered
                        if (MessageBox.Show("Keine Antwort gegeben.\nWollen sie weiterspielen?", "Keine Antwort", MessageBoxButton.YesNo) == MessageBoxResult.No)
                        {
                            timer.Stop();
                            _view.DialogResult = true;
                            _view.Close();
                        }
                        else
                        {
                            NextRound();
                        }
                    }
                    else
                    {
                        //Answered
                        if (MessageBox.Show(valid.Message + "\nWollen sie weiterspielen?", valid.Title, MessageBoxButton.YesNo) == MessageBoxResult.No)
                        {
                            timer.Stop();
                            _view.DialogResult = true;
                            _view.Close();
                        }
                        else
                        {
                            if (valid.Correct == true ? true : false)
                            {
                                _viewModel.Correct++;
                                _viewModel.Score = valid.Score;
                            }
                            NextRound();
                        }
                    }
                }
                else
                {
                    //Cheated
                    MessageBox.Show("Schummeln ist nicht erlaubt! Das Spiel wird abgebrochen.");
                    _view.DialogResult = true;
                    _view.Close();
                }

            }
            catch (Exception)
            {
                MessageBox.Show(
                    "Verbindungsfehler. Dieser kann auftreten, wenn sie sich länger als 15 Minuten nicht beim Server gemeldet haben oder die Verbindung zum Server abgebrochen ist.");
                _view.DialogResult = false;
                _view.Close();
            }
        }

        //Cancel game
        private void CancelCommandExecute(object obj)
        {
                timer.Stop();
                _view.DialogResult = true;
                _view.Close();
        }

        //Reset countdown
        private void InitializeCountDown()
        {
            timer = new DispatcherTimer();
            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = TimeSpan.FromMilliseconds(500);
            timer.Start();
        }

        //Start next round
        private void NextRound()
        {
            _viewModel.Count++;

            if (_viewModel.Count < QuizConfiguration.QUESTION_COUNT)
            {
                //More questions comming
                try
                {
                    //Get next question
                    QuestionSubmit question = _quizService.GetQuestion(_viewModel.Count);
                
                    if (question != null)
                    {
                        _viewModel.Question = question;
                        _viewModel.CountDown = QuizConfiguration.COUNTDOWN;

                        //Reset countdown
                        InitializeCountDown();
                    }
                    else
                    {
                        MessageBox.Show("Fehler beim Laden der Fragen. Das Spiel wird leider abgebrochen.");
                    }
                    _viewModel.Answer = "";
                }
                catch (Exception)
                {
                    MessageBox.Show("Verbindungsfehler. Dieser kann auftreten, wenn sie sich länger als 15 Minuten nicht beim Server gemeldet haben oder die Verbindung zum Server abgebrochen ist.");
                    _view.DialogResult = false;
                    _view.Close();
                }
            }
            else
            {
                //Quiz is over
                try
                {
                    //Save Score on selected in user
                    _quizService.EndOfGame();

                    MessageBox.Show("Es waren " + _viewModel.Correct + " von " + QuizConfiguration.QUESTION_COUNT +
                                      " Fragen richtig. \nGesamtpunktzahl: " + _viewModel.Score);
                    _view.DialogResult = true;
                    _view.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("Verbindungsfehler. Dieser kann auftreten, wenn sie sich länger als 15 Minuten nicht beim Server gemeldet haben oder die Verbindung zum Server abgebrochen ist.");
                    _view.DialogResult = false;
                    _view.Close();
                }
            }
        }

        //Decrement timer every 500 miliseconds
        private void timer_Tick(object sender, EventArgs e)
        {
            if (_viewModel.CountDown > 0)
            {
                //Decrement countdown
                _viewModel.CountDown = _viewModel.CountDown - 500;
                //Convert to seconds
                _viewModel.CountDownShown = _viewModel.CountDown / 1000;
            }
            else
            {
                //Countdown time is over
                timer.Stop();

                if (MessageBox.Show("Keine Antwort abgegeben \nWollen sie weiterspielen?", "Falsch!", MessageBoxButton.YesNo) == MessageBoxResult.No)
                {
                    _view.DialogResult = true;
                    _view.Close();
                }
                else
                {
                    NextRound();
                }
            }
        }
    }
}
