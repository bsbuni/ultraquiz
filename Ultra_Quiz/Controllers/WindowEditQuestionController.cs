﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using FluentNHibernate.Conventions;
using NHibernate.Linq;
using Ultra_Quiz.Configuration;
using Ultra_Quiz.Framework;
using Ultra_Quiz.Models;
using Ultra_Quiz.ViewModels;
using Ultra_Quiz.Views;

namespace Ultra_Quiz.Controllers
{
    public class WindowEditQuestionController
    {
        private Question _selectedQuestionModel;
        private WindowEditQuestion _view;
        private WindowEditQuestionViewModel _viewModel;

        private Repository<Category> _categoryRepository;
        private Repository<Level> _levelRepository;
        private Repository<QuestionToCategory> _questionToCategoryRepository;

        private ObservableCollection<Category> _categories;
        private ObservableCollection<Level> _levels;
        private ObservableCollection<QuestionToCategory> _questionsToCategories;

        public bool NewQuestion()
        {
            return EditQuestion(new Question());
        }

        public bool EditQuestion(Question selectedQuestionModel)
        {
            _view = new WindowEditQuestion();

            _selectedQuestionModel = selectedQuestionModel;

            _levelRepository = new Repository<Level>(QuizConfiguration.DATABASE_PATH);
            _questionToCategoryRepository = new Repository<QuestionToCategory>(QuizConfiguration.DATABASE_PATH);
            _categoryRepository = new Repository<Category>(QuizConfiguration.DATABASE_PATH);

            _levels = new ObservableCollection<Level>(_levelRepository.GetAll());
            _questionsToCategories =
                new ObservableCollection<QuestionToCategory>(_questionToCategoryRepository.GetAll());
            _categories = new ObservableCollection<Category>(_categoryRepository.GetAll());

            //Get all QuestionToCategory-Models of this Question
            var addedCategories = new List<QuestionToCategory>(from questionsToCategory in _questionsToCategories
                where questionsToCategory.Question.Id == selectedQuestionModel.Id
                select questionsToCategory);
            //List of Ids of all Categories linked to the Questionn
            var addedCategoriesIds = new List<int>(addedCategories.Select(x => x.Category.Id));


            //Create List of new QuestionToCategory-Models of all Categories, which are not linked to the Question
            var removedCategories =
                new List<QuestionToCategory>(_categories.Where(x => !addedCategoriesIds.Contains(x.Id)).Select(x =>
                {
                    var questionToCategory = new QuestionToCategory
                    {
                        Question = selectedQuestionModel,
                        Category = x
                    };
                    return questionToCategory;
                }
                    ));

            _viewModel = new WindowEditQuestionViewModel
            {
                Model = selectedQuestionModel,
                Levels = _levels,
                Added = new ObservableCollection<QuestionToCategory>(addedCategories),
                Removed = new ObservableCollection<QuestionToCategory>(removedCategories),
                AddCommand = new RelayCommand(AddCommandExecute),
                RemoveCommand = new RelayCommand(RemoveCommandExecute),
                OkCommand = new RelayCommand(ExecuteOkCommand),
                CancelCommand = new RelayCommand(ExecuteCancelCommand)
            };

            _view.DataContext = _viewModel;

            return _view.ShowDialog() == true ? true : false;
        }

        private void ExecuteOkCommand(object obj)
        {
            if (string.IsNullOrEmpty(_viewModel.QuestionString) || string.IsNullOrEmpty(_viewModel.Answer) ||
                _viewModel.Level == null)
            {
                MessageBox.Show("Frage und Antwort sowie ein Level müssen angegeben sein.");
            }
            else if (_viewModel.QuestionString.Length > 200 || _viewModel.Answer.Length > 50 || (_viewModel.AnswerRegEx != null && _viewModel.AnswerRegEx.Length > 50))
            {
                MessageBox.Show("Die Frage darf nicht länger als 200 Zeichen sein. Die Einschränkung für die Antwort und den regulären Ausdruck sind 50 Zeichnen");
            }
            else
            {
                var questionRepository = new Repository<Question>(QuizConfiguration.DATABASE_PATH);
                var questions = new ObservableCollection<Question>(questionRepository.GetAll());

                questions = new ObservableCollection<Question>(from question in questions
                    where question.QuestionString.Equals(_viewModel.QuestionString)
                    select question);

                if (_selectedQuestionModel.Id == 0 && questions.IsNotEmpty())
                {
                    MessageBox.Show("Die Frage ist bereits vorhanden.");
                }
                else
                {
                    if (_viewModel.Added.IsNotEmpty())
                    {
                        questionRepository.Save(_selectedQuestionModel);
                        ProcessChanges(_selectedQuestionModel);
                        
                        _view.DialogResult = true;
                        _view.Close();
                    }
                    else
                    {
                        MessageBox.Show("Es muss mindestens eine Kategorie ausgewählt sein.");
                    }
                }
            }
        }

        private void ExecuteCancelCommand(object obj)
        {
            _view.DialogResult = false;
            _view.Close();
        }

        private void AddCommandExecute(object obj)
        {
            if (_viewModel.AddSelected != null)
            {
                _viewModel.Added.Add(_viewModel.AddSelected);
                _viewModel.Removed.Remove(_viewModel.AddSelected);
            }
        }

        private void RemoveCommandExecute(object obj)
        {
            if (_viewModel.RemoveSelected != null)
            {
                _viewModel.Removed.Add(_viewModel.RemoveSelected);
                _viewModel.Added.Remove(_viewModel.RemoveSelected);
            }
        }

        private void ProcessChanges(Question question)
        {
            //Delete all persisted QuestionToCategory-Models, which are to be removed
            _viewModel.Removed.Where(x => x.Id != 0).ForEach(_questionToCategoryRepository.Delete);

            //Save or Update all QuestionToCategory-Model for the Question
            _viewModel.Added.ForEach(_questionToCategoryRepository.Save);
        }
    }
}