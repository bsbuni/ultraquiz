﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ultra_Quiz.Models
{
    public class QuestionToCategory : ModelBase
    {
        private Category _category;

        public Category Category
        {
            get { return _category; }
            set { _category = value; }
        }

        private Question _question;

        public Question Question
        {
            get { return _question; }
            set { _question = value; }
        }

        public override string ToString()
        {
            return _category.Name;
        }
    }
}
