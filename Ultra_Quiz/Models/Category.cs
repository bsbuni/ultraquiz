﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ultra_Quiz.Models
{
    public class Category : ModelBase
    {
        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private string _description;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        private IList<QuestionToCategory> _questions;

        public IList<QuestionToCategory> Questions
        {
            get { return _questions; }
            set { _questions = value; }
        }

        public override string ToString()
        {
            return _name;
        }
    }
}
