﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ultra_Quiz.Models
{
    public class Question : ModelBase
    {
        private string _questionString;

        public string QuestionString
        {
            get { return _questionString; }
            set { _questionString = value; }
        }

        private string _answer;

        public string Answer
        {
            get { return _answer; }
            set { _answer = value; }
        }
        private string _answerRegEx;

        public string AnswerRegEx
        {
            get { return _answerRegEx; }
            set { _answerRegEx = value; }
        }
        private Level _level;

        public Level Level
        {
            get { return _level; }
            set { _level = value; }
        }

        private IList<QuestionToCategory> _categories;

        public IList<QuestionToCategory> Categories
        {
            get { return _categories; }
            set { _categories = value; }
        }

        public override string ToString()
        {
            return _questionString;
        }
    }
}
