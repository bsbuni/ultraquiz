﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ultra_Quiz.Models
{
    public class Level : ModelBase
    {
        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private int _score;

        public int Score
        {
            get { return _score; }
            set { _score = value; }
        }

        public override string ToString()
        {
            return _name;
        }
    }
}
