﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ultra_Quiz.Models
{
    public class User : ModelBase
    {
        private string _name;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private int _totalScore;

        public int TotalScore
        {
            get { return _totalScore; }
            set { _totalScore = value; }
        }

        public override string ToString()
        {
            return _name;
        }
    }
}
