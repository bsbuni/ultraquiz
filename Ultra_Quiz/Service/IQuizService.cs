﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.ServiceModel;
using Ultra_Quiz.Models;

namespace Ultra_Quiz.Service
{
    [ServiceContract(SessionMode = SessionMode.Required, Name = "QuizService")]
    public interface IQuizService
    {
        [OperationContract]  
        QuestionsSubmit GetQuestions(int categoryId, int levelId);

        [OperationContract]  
        QuestionSubmit GetQuestion(int id);

        [OperationContract]  
        ValidationSubmit CheckQuestion(int countdown, int id, string answer);

        [OperationContract]
        void EndOfGame();

        [OperationContract]
        bool SetUser(UserSubmit userSubmit);

        [OperationContract]
        UsersSubmit GetHighscore();

        [OperationContract]  
        UsersSubmit GetUsers();

        [OperationContract]
        CategoriesSubmit GetCateories();
        
        [OperationContract]
        LevelsSubmit GetLevels();
    }

    [DataContract]
    public class UsersSubmit
    {
        [DataMember]
        public ObservableCollection<UserSubmit> Users { get; set; }
    }

    [DataContract]
    public class UserSubmit
    {
        public UserSubmit(User user)
        {
            Name = user.Name;
            TotalScore = user.TotalScore;
            Id = user.Id;
        }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int TotalScore { get; set; }

        [DataMember]
        public int Id { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }

    [DataContract]
    public class QuestionsSubmit
    {
        [DataMember]
        public ObservableCollection<QuestionSubmit> Questions { get; set; }
    }

    [DataContract]
    public class QuestionSubmit
    {
        public QuestionSubmit(Question question)
        {
            QuestionString = question.QuestionString;
            Id = question.Id;
        }

        [DataMember]
        public string QuestionString { get; set; }

        [DataMember]
        public int Id { get; set; }

        public override string ToString()
        {
            return QuestionString;
        }
    }

    [DataContract]
    public class LevelsSubmit
    {
        [DataMember]
        public ObservableCollection<LevelSubmit> Levels { get; set; }
    }

    [DataContract]
    public class LevelSubmit
    {
        public LevelSubmit(Level level)
        {
            Name = level.Name;
            Score = level.Score;
            Id = level.Id;
        }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int Score { get; set; }

        [DataMember]
        public int Id { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }

    [DataContract]
    public class CategoriesSubmit
    {
        [DataMember]
        public ObservableCollection<CategorySubmit> Categories { get; set; }
    }

    [DataContract]
    public class CategorySubmit
    {
        public CategorySubmit(Category category)
        {
            Name = category.Name;
            Description = category.Description;
            Id = category.Id;
        }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int Id { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }

    [DataContract]
    public class ValidationSubmit
    {
        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Message { get; set; }
        
        [DataMember]
        public bool? Correct { get; set; }

        [DataMember]
        public int Score { get; set; }
    }
}
