﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text.RegularExpressions;
using System.Windows.Documents;
using FluentNHibernate.Utils;
using NHibernate.Linq;
using Ultra_Quiz.Configuration;
using Ultra_Quiz.Framework;
using Ultra_Quiz.Models;

namespace Ultra_Quiz.Service
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]
    public class QuizService : IQuizService
    {
        private int _score;
        private User _user;

        private ObservableCollection<Category> _categories;
        private ObservableCollection<Level> _levels;
        private ObservableCollection<Question> _questions;
        private ObservableCollection<User> _users;
        private ObservableCollection<QuestionToCategory> _questionsToCategories;
        private ObservableCollection<Question> _quizQuestions; 

        public QuestionsSubmit GetQuestions(int categoryId, int levelId)
        {
            var questionRepository = new Repository<Question>(QuizConfiguration.DATABASE_PATH);
            _questions = new ObservableCollection<Question>(questionRepository.GetAll());

            //Filter questions
            var filteredQuestions = FilterBy(_questions, categoryId, levelId);
            _quizQuestions = GetQuizQuestions(filteredQuestions);

            //Get random questions
            var quizQuestionsSubmit= _quizQuestions != null ? 
                new ObservableCollection<QuestionSubmit>(_quizQuestions.Select(x => new QuestionSubmit(x))) : null;

            return new QuestionsSubmit
            {
                Questions = quizQuestionsSubmit
            };
        }

        private ObservableCollection<Question> FilterBy(ObservableCollection<Question> questions, int categoryId, int levelId)
        {
            //Filter all questions by category, if category is set
            if (categoryId != 0)
            {
                var questionToCategoryRepository =
                    new Repository<QuestionToCategory>(QuizConfiguration.DATABASE_PATH);
                _questionsToCategories =
                    new ObservableCollection<QuestionToCategory>(questionToCategoryRepository.GetAll());
                questions = new ObservableCollection<Question>(from questionCategory in _questionsToCategories
                    where questionCategory.Category.Id == categoryId
                    select questionCategory.Question);
            }

            //Filter previously given questions by level, if level is given
            if (levelId != 0)
            {
                questions = new ObservableCollection<Question>(from question in questions
                    where question.Level != null && question.Level.Id == levelId
                    select question);
            }

            return questions;
        }

        private ObservableCollection<Question> GetQuizQuestions(ObservableCollection<Question> questions)
        {
            if (questions.Count < QuizConfiguration.QUESTION_COUNT)
            {
                return null;
            }

            var quizQuestions = new ObservableCollection<Question>();
            var random = new Random();

            //Get random questions
            for (int i = 0; i < QuizConfiguration.QUESTION_COUNT; i++)
            {
                var question = new Question();
                bool found = false;
                while (!found)
                {
                    //Get random question and check whether it is allready a selected candidate
                    int index = random.Next(questions.Count);
                    question = questions[index];
                    if (!quizQuestions.Contains(question))
                    {
                        found = true;
                    }
                }
                quizQuestions.Add(question);
            }

            if (quizQuestions.Count != QuizConfiguration.QUESTION_COUNT)
            {
                return null;
            }

            return quizQuestions;
        }

        public QuestionSubmit GetQuestion(int index)
        {
            if (_quizQuestions.Count == 0)
            {
                return null;
            }

            Question question = _quizQuestions[index];
            return new QuestionSubmit(question);
        }

        public ValidationSubmit CheckQuestion(int countdown, int index, string answer)
        {
            if (string.IsNullOrEmpty(answer))
            {
                //Empty answer
                return new ValidationSubmit
                {
                    Title = "Keine Antwort",
                    Message = "Keine Antwort gegeben.\nWollen sie weiterspielen?", 
                    Correct = null,
                    Score = _score
                };
            }
            else if (countdown >= QuizConfiguration.COUNTDOWN - 500)
            {
                return null;
            }
            else
            {
                var question = _quizQuestions[index];
                
                if (ValidateAnswer(question, answer))
                {
                    _score += question.Level.Score;
                    return new ValidationSubmit
                    {
                        Title = "Richtig!",
                        Message = "Die Antwort \"" + answer + "\" zur Frage \"" +
                                  question.QuestionString + "\" ist richtig.",
                        Correct = true,
                        Score = _score
                    };
                }
                else
                {
                    return new ValidationSubmit
                    {
                        Title = "Falsch...",
                        Message = "Die Antwort \"" + answer + "\" zur Frage \"" +
                               question.QuestionString + "\" ist falsch. Die richtige Antwort wäre \"" +
                               question.Answer + "\" gewesen.",
                        Correct = false,
                        Score = _score
                    };
                }
            }
        }

        public void EndOfGame()
        {
            //End game and update users score
            var userRepository = new Repository<User>(QuizConfiguration.DATABASE_PATH);
            _user.TotalScore += _score;
            userRepository.Save(_user);
        }

        //Set selected user for playing
        public bool SetUser(UserSubmit userSubmit)
        {
            //TODO Check if user is allready logged in
            var userRepository = new Repository<User>(QuizConfiguration.DATABASE_PATH);
            _users = new ObservableCollection<User>(userRepository.GetAll());

            _user = (from user in _users
                where user.Id == userSubmit.Id
                select user).ToList()[0];

            return true;
        }

        //Validate answer
        private bool ValidateAnswer(Question question, string answer)
        {
            answer = answer.Trim();
            answer = answer.ToUpper();
            answer = answer.Replace("Ä", "AE");
            answer = answer.Replace("Ö", "OE");
            answer = answer.Replace("Ü", "UE");
            answer = answer.Replace("ß", "SS");

            if (question.AnswerRegEx != null)
            {
                string manipulatedRegex = question.AnswerRegEx.ToUpper();

                manipulatedRegex = manipulatedRegex.Replace("Ä", "(AE)");
                manipulatedRegex = manipulatedRegex.Replace("Ö", "(OE)");
                manipulatedRegex = manipulatedRegex.Replace("Ü", "(UE)");
                manipulatedRegex = manipulatedRegex.Replace("ß", "(SS)");

                var regex = new Regex(manipulatedRegex);
                return regex.IsMatch(answer);
            }
            else
            {
                string manipulatedAnswer = question.Answer.ToUpper();

                manipulatedAnswer = manipulatedAnswer.Replace("Ä", "AE");
                manipulatedAnswer = manipulatedAnswer.Replace("Ö", "OE");
                manipulatedAnswer = manipulatedAnswer.Replace("Ü", "UE");
                manipulatedAnswer = manipulatedAnswer.Replace("ß", "SS");

                return answer.Equals(manipulatedAnswer, StringComparison.CurrentCulture);
            }
        }

        public UsersSubmit GetHighscore()
        {
            var users = this.GetUsers().Users;
            return new UsersSubmit
            {
                Users = new ObservableCollection<UserSubmit>(users.OrderByDescending(x => x.TotalScore).ToList())
            };
        }

        public UsersSubmit GetUsers()
        {
            var userRepository = new Repository<User>(QuizConfiguration.DATABASE_PATH);
            _users = new ObservableCollection<User>(userRepository.GetAll());
            return new UsersSubmit
            {
                Users = new ObservableCollection<UserSubmit>(_users.Select(x => new UserSubmit(x)))
            };
        }

        public CategoriesSubmit GetCateories()
        {
             var categoryRepository = new Repository<Category>(QuizConfiguration.DATABASE_PATH);
            _categories = new ObservableCollection<Category>(categoryRepository.GetAll());
        
            return new CategoriesSubmit
            {
                Categories = new ObservableCollection<CategorySubmit>(_categories.Select(x => new CategorySubmit(x)))
            };
        }

        public LevelsSubmit GetLevels()
        {
            var levelRepository = new Repository<Level>(QuizConfiguration.DATABASE_PATH);
            _levels = new ObservableCollection<Level>(levelRepository.GetAll());
            return new LevelsSubmit
            {
                Levels = new ObservableCollection<LevelSubmit>(_levels.Select(x => new LevelSubmit(x)))
            };
        }
    }
}
