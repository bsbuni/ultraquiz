﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Ultra_Quiz.Framework;
using Ultra_Quiz.Models;
using Ultra_Quiz.Service;

namespace Ultra_Quiz.ViewModels
{
    public class HighscoreWindowViewModel : ViewModelBase
    {
        private ObservableCollection<UserSubmit> _users = new ObservableCollection<UserSubmit>();
        public ObservableCollection<UserSubmit> Users
        {
            get { return _users; }
            set
            {
                if (_users == value)
                    return;

                _users = value;
                OnPropertyChanged("Users");
            }
        }

        private ICommand _closeCommand;
        public ICommand CloseCommand
        {
            get { return _closeCommand; }
            set { _closeCommand = value; }
        }
    }
}
