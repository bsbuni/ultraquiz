﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Ultra_Quiz.Framework;
using Ultra_Quiz.Models;

namespace Ultra_Quiz.ViewModels
{
    class WindowEditQuestionViewModel : ViewModelBase
    {
        private Question _model;
        public Question Model
        {
            get { return _model; }
            set { _model = value; }
        }

        private ObservableCollection<Level> _levels;
        public ObservableCollection<Level> Levels
        {
            get { return _levels; }
            set { _levels = value; }
        }

        private ICommand _okCommand;
        public ICommand OkCommand
        {
            get { return _okCommand; }
            set { _okCommand = value; }
        }

        private ICommand _cancelCommand;
        public ICommand CancelCommand
        {
            get { return _cancelCommand; }
            set { _cancelCommand = value; }
        }

        private ICommand _addCommand;
        public ICommand AddCommand
        {
            get { return _addCommand; }
            set { _addCommand = value; }
        }

        private ICommand _removeCommand;
        public ICommand RemoveCommand
        {
            get { return _removeCommand; }
            set { _removeCommand = value; }
        }

        private ObservableCollection<QuestionToCategory> _added;
        public ObservableCollection<QuestionToCategory> Added
        {
            get { return _added; }
            set
            {
                if (_added == value)
                    return;

                _added = value;
                OnPropertyChanged("Added");
            }
        }

        private ObservableCollection<QuestionToCategory> _removed;
        public ObservableCollection<QuestionToCategory> Removed
        {
            get { return _removed; }
            set
            {
                if (_removed == value)
                    return;

                _removed = value;
                OnPropertyChanged("Removed");
            }
        } 
        
        private QuestionToCategory _addSelected;
        public QuestionToCategory AddSelected
        {
            get { return _addSelected; }
            set
            {
                if (_addSelected == value)
                    return;

                _addSelected = value;
                OnPropertyChanged("AddSelected");
            }
        }

        private QuestionToCategory _removeSelected;
        public QuestionToCategory RemoveSelected
        {
            get { return _removeSelected; }
            set
            {
                if (_removeSelected == value)
                    return;

                _removeSelected = value;
                OnPropertyChanged("RemoveSelected");
            }
        }

        public Level Level
        {
            get { return Model.Level; }
            set
            {
                if (Model.Level == value)
                    return;

                Model.Level = value;
                OnPropertyChanged("Level");
            }
        }

        public string QuestionString
        {
            get { return Model.QuestionString; }
            set
            {
                if (Model.QuestionString == value)
                    return;

                Model.QuestionString = value;
                OnPropertyChanged("QuestionString");
            }
        }

        public string Answer
        {
            get { return Model.Answer; }
            set
            {
                if (Model.Answer == value)
                    return;

                Model.Answer = value;
                OnPropertyChanged("Answer");
            }
        }

        public string AnswerRegEx
        {
            get { return Model.AnswerRegEx; }
            set
            {
                if (Model.AnswerRegEx == value)
                    return;

                Model.AnswerRegEx = value;
                OnPropertyChanged("AnswerRegEx");
            }
        }
    }
}
