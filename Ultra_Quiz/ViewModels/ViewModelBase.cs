﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ultra_Quiz.Framework
{
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChangedEventArgs propertyChangedEventArgs = new PropertyChangedEventArgs(propertyName);
                PropertyChanged.Invoke(this,propertyChangedEventArgs);
            }
        }
    }
}
