﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Ultra_Quiz.Framework;
using Ultra_Quiz.Models;
using Ultra_Quiz.Service;

namespace Ultra_Quiz.ViewModels
{
    public class PlayWindowViewModel : ViewModelBase
    {
        private ObservableCollection<QuestionSubmit> _questions = new ObservableCollection<QuestionSubmit>();
        public ObservableCollection<QuestionSubmit> Questions
        {
            get { return _questions; }
            set
            {
                if (_questions == value)
                    return;

                _questions = value;
                OnPropertyChanged("Questions");
            }
        }

        private int _count;

        public int Count
        {
            get { return _count; }
            set
            {
                if (_count == value)
                    return;

                _count = value;
                OnPropertyChanged("Count");
                OnPropertyChanged("CountShown");
            }
        }

        public int CountShown
        {
            get { return _count +1; }
        }

        private int _correct;
        public int Correct
        {
            get { return _correct; }
            set
            {
                if (_correct == value)
                    return;

                _correct = value;
                OnPropertyChanged("Correct");
            }
        }

        private int _score;
        public int Score
        {
            get { return _score; }
            set
            {
                if (_score == value)
                    return;

                _score = value;
                OnPropertyChanged("Score");
            }
        }

        private QuestionSubmit _question;
        public QuestionSubmit Question
        {
            get { return _question; }
            set
            {
                if (_question == value)
                    return;

                _question = value;
                OnPropertyChanged("Question");
            }
        }

        private int _countDown;
        public int CountDown
        {
            get { return _countDown; }
            set
            {
                if (_countDown == value)
                    return;

                _countDown = value;
                OnPropertyChanged("CountDown");
            }
        }

        private int _countDownShown;
        public int CountDownShown
        {
            get { return _countDownShown; }
            set
            {
                if (_countDownShown == value)
                    return;

                _countDownShown = value;
                OnPropertyChanged("CountDownShown");
            }
        }

        private UserSubmit _user;
        public UserSubmit User
        {
            get { return _user; }
            set
            {
                if (_user == value)
                    return;

                _user = value;
                OnPropertyChanged("User");
            }
        }

        private string _answer;

        public string Answer
        {
            get { return _answer; }
            set
            {
                if (_answer == value)
                    return;

                _answer = value;
                OnPropertyChanged("Answer");
            }
        }

        private ICommand _answerCommand;
        public ICommand AnswerCommand
        {
            get { return _answerCommand; }
            set { _answerCommand = value; }
        }

        private ICommand _cancelCommand;
        public ICommand CancelCommand
        {
            get { return _cancelCommand; }
            set { _cancelCommand = value; }
        }
    }
}
