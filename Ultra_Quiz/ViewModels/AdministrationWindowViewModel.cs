﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Ultra_Quiz.Framework;
using Ultra_Quiz.Models;

namespace Ultra_Quiz.ViewModels
{
    class AdministrationWindowViewModel : ViewModelBase
    {
        private ICommand _updateCommand;
        public ICommand UpdateCommand
        {
            get { return _updateCommand; }
            set { _updateCommand = value; }
        }

        private ICommand _closeCommand;
        public ICommand CloseCommand
        {
            get { return _closeCommand; }
            set { _closeCommand = value; }
        }

        //Question administration tab---------------------------------------------------------------------------------------------------------
        private ObservableCollection<Question> _questionModels = new ObservableCollection<Question>();
        public ObservableCollection<Question> QuestionModels
        {
            get { return _questionModels; }
            set
            {
                if (_questionModels == value)
                    return;

                _questionModels = value;
                OnPropertyChanged("QuestionModels");
            }
        }

        private Question _selectedQuestionModel;
        public Question SelectedQuestionModel
        {
            get { return _selectedQuestionModel; }
            set
            {
                if (_selectedQuestionModel == value)
                    return;

                _selectedQuestionModel = value;
                OnPropertyChanged("SelectedQuestionModel");
            }
        }
        
        private ICommand _newQuestionCommand;
        public ICommand NewQuestionCommand
        {
            get { return _newQuestionCommand; }
            set { _newQuestionCommand = value; }
        }

        private ICommand _editQuestionCommand;
        public ICommand EditQuestionCommand
        {
            get { return _editQuestionCommand; }
            set { _editQuestionCommand = value; }
        }

        private ICommand _deleteQuestionCommand;
        public ICommand DeleteQuestionCommand
        {
            get { return _deleteQuestionCommand; }
            set { _deleteQuestionCommand = value; }
        }

        private ICommand _importQuestionCommand;
        public ICommand ImportQuestionCommand
        {
            get { return _importQuestionCommand; }
            set { _importQuestionCommand = value; }
        }

        //Category administration tab---------------------------------------------------------------------------------------------------------
        private ObservableCollection<Category> _categoryModels = new ObservableCollection<Category>();
        public ObservableCollection<Category> CategoryModels
        {
            get { return _categoryModels; }
            set
            {
                if (_categoryModels == value)
                    return;

                _categoryModels = value;
                OnPropertyChanged("CategoryModels");
            }
        }

        private Category _selectedCategoryModel;
        public Category SelectedCategoryModel
        {
            get { return _selectedCategoryModel; }
            set
            {
                if (_selectedCategoryModel == value)
                    return;

                _selectedCategoryModel = value;
                OnPropertyChanged("SelectedCategoryModel");
            }
        }

        private ICommand _newCategoryCommand;
        public ICommand NewCategoryCommand
        {
            get { return _newCategoryCommand; }
            set { _newCategoryCommand = value; }
        }

        private ICommand _editCategoryCommand;
        public ICommand EditCategoryCommand
        {
            get { return _editCategoryCommand; }
            set { _editCategoryCommand = value; }
        }

        private ICommand _deleteCategoryCommand;
        public ICommand DeleteCategoryCommand
        {
            get { return _deleteCategoryCommand; }
            set { _deleteCategoryCommand = value; }
        }

        //Level administration tab---------------------------------------------------------------------------------------------------------
        private ObservableCollection<Level> _levelModels = new ObservableCollection<Level>();
        public ObservableCollection<Level> LevelModels
        {
            get { return _levelModels; }
            set
            {
                if (_levelModels == value)
                    return;

                _levelModels = value;
                OnPropertyChanged("LevelModels");
            }
        }

        private Level _selectedLevelModel;
        public Level SelectedLevelModel
        {
            get { return _selectedLevelModel; }
            set
            {
                if (_selectedLevelModel == value)
                    return;

                _selectedLevelModel = value;
                OnPropertyChanged("SelectedLevelModel");
            }
        }

        private ICommand _newLevelCommand;
        public ICommand NewLevelCommand
        {
            get { return _newLevelCommand; }
            set { _newLevelCommand = value; }
        }

        private ICommand _editLevelCommand;
        public ICommand EditLevelCommand
        {
            get { return _editLevelCommand; }
            set { _editLevelCommand = value; }
        }

        private ICommand _deleteLevelCommand;
        public ICommand DeleteLevelCommand
        {
            get { return _deleteLevelCommand; }
            set { _deleteLevelCommand = value; }
        }

        //User administration tab---------------------------------------------------------------------------------------------------------
        private ObservableCollection<User> _userModels = new ObservableCollection<User>();
        public ObservableCollection<User> UserModels
        {
            get { return _userModels; }
            set
            {
                if (_userModels == value)
                    return;

                _userModels = value;
                OnPropertyChanged("UserModels");
            }
        }

        private User _selectedUserModel;
        public User SelectedUserModel
        {
            get { return _selectedUserModel; }
            set
            {
                if (_selectedUserModel == value)
                    return;

                _selectedUserModel = value;
                OnPropertyChanged("SelectedUserModel");
            }
        }

        private ICommand _newUserCommand;
        public ICommand NewUserCommand
        {
            get { return _newUserCommand; }
            set { _newUserCommand = value; }
        }

        private ICommand _editUserCommand;
        public ICommand EditUserCommand
        {
            get { return _editUserCommand; }
            set { _editUserCommand = value; }
        }

        private ICommand _deleteUserCommand;
        public ICommand DeleteUserCommand
        {
            get { return _deleteUserCommand; }
            set { _deleteUserCommand = value; }
        }
    }
}
