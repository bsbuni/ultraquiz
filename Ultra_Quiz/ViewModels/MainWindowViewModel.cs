﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Ultra_Quiz.Framework;
using Ultra_Quiz.Models;
using Ultra_Quiz.Service;

namespace Ultra_Quiz.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        private ObservableCollection<UserSubmit> _users;
        public ObservableCollection<UserSubmit> Users
        {
            get { return _users; }
            set
            {
                if (_users == value)
                    return;

                _users = value;
                OnPropertyChanged("Users");
            }
        }

        private ObservableCollection<LevelSubmit> _levels;
        public ObservableCollection<LevelSubmit> Levels
        {
            get { return _levels; }
            set
            {
                if (_levels == value)
                    return;

                _levels = value;
                OnPropertyChanged("Levels");
            }
        }

        private ObservableCollection<CategorySubmit> _categories;
        public ObservableCollection<CategorySubmit> Categories
        {
            get { return _categories; }
            set
            {
                if (_categories == value)
                    return;

                _categories = value;
                OnPropertyChanged("Categories");
            }
        }

        private UserSubmit _user;
        public UserSubmit User
        {
            get { return _user; }
            set
            {
                if (_user == value)
                    return;

                _user = value;
                OnPropertyChanged("User");
            }
        }

        private CategorySubmit _category;
        public CategorySubmit Category
        {
            get { return _category; }
            set
            {
                if (_category == value)
                    return;

                _category = value;
                OnPropertyChanged("Category");
            }
        }

        private LevelSubmit _level;
        public LevelSubmit Level
        {
            get { return _level; }
            set
            {
                if (_level == value)
                    return;

                _level = value;
                OnPropertyChanged("Level");
            }
        }

        private string _switchHostText;
        public string SwitchHostText
        {
            get { return _switchHostText; }
            set
            {
                if (_switchHostText == value)
                    return;

                _switchHostText = value;
                OnPropertyChanged("SwitchHostText");
            }
        }

        private string _switchConnectText;
        public string SwitchConnectText
        {
            get { return _switchConnectText; }
            set
            {
                if (_switchConnectText == value)
                    return;

                _switchConnectText = value;
                OnPropertyChanged("SwitchConnectText");
            }
        }

        private ICommand _administrationCommand;
        public ICommand AdministrationCommand
        {
            get { return _administrationCommand; }
            set { _administrationCommand = value; }
        }

        private ICommand _playCommand;

        public ICommand PlayCommand
        {
            get { return _playCommand; }
            set { _playCommand = value; }
        }

        private ICommand _highscoreoCommand;

        public ICommand HighscoreCommand
        {
            get { return _highscoreoCommand; }
            set { _highscoreoCommand = value; }
        }

        private ICommand _updateCommand;

        public ICommand UpdateCommand
        {
            get { return _updateCommand; }
            set { _updateCommand = value; }
        }

        private ICommand _switchHostCommand;
        public ICommand SwitchHostCommand
        {
            get { return _switchHostCommand; }
            set { _switchHostCommand = value; }
        }

        private ICommand _switchConnectCommand;
        public ICommand SwitchConnectCommand
        {
            get { return _switchConnectCommand; }
            set { _switchConnectCommand = value; }
        }
    }
}
