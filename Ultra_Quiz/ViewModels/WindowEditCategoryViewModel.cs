﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Ultra_Quiz.Framework;
using Ultra_Quiz.Models;

namespace Ultra_Quiz.ViewModels
{
    internal class WindowEditCategoryViewModel : ViewModelBase
    {
        private Category _model;

        public Category Model
        {
            get { return _model; }
            set { _model = value; }
        }

        private ICommand _okCommand;

        public ICommand OkCommand
        {
            get { return _okCommand; }
            set { _okCommand = value; }
        }

        private ICommand _cancelCommand;

        public ICommand CancelCommand
        {
            get { return _cancelCommand; }
            set { _cancelCommand = value; }
        }

        public string Name
        {
            get { return Model.Name; }
            set
            {
                if (Model.Name == value)
                    return;

                Model.Name = value;
                OnPropertyChanged("Name");
            }
        }

        public string Description
        {
            get { return Model.Description; }
            set
            {
                if (Model.Description == value)
                    return;

                Model.Description = value;
                OnPropertyChanged("Description");
            }
        }
    }
}
