﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Ultra_Quiz.Framework;
using Ultra_Quiz.Models;

namespace Ultra_Quiz.ViewModels
{
    internal class WindowEditLevelViewModel : ViewModelBase
    {
        private Level _model;

        public Level Model
        {
            get { return _model; }
            set { _model = value; }
        }

        private ICommand _okCommand;

        public ICommand OkCommand
        {
            get { return _okCommand; }
            set { _okCommand = value; }
        }

        private ICommand _cancelCommand;

        public ICommand CancelCommand
        {
            get { return _cancelCommand; }
            set { _cancelCommand = value; }
        }

        public string Name
        {
            get { return Model.Name; }
            set
            {
                if (Model.Name == value)
                    return;

                Model.Name = value;
                OnPropertyChanged("Name");
            }
        }

        public int Score
        {
            get { return Model.Score; }
            set
            {
                if (Model.Score == value)
                    return;

                Model.Score = value;
                OnPropertyChanged("Score");
            }
        }
    }
}
