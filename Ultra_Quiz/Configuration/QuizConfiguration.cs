﻿namespace Ultra_Quiz.Configuration
{
    //Containing the configuration data for the ultra quiz application
    public class QuizConfiguration
    {
        public const int SESSION_TIMEOUT = 15; //Declaration in minutes
        public const int QUESTION_COUNT = 20;
        public const int COUNTDOWN = 20000; //Declaration in milliseconds
        public const string DATABASE_PATH = @"Database\ultraquiz_gefuellt.db3";
        public const string SERVICE_HOST = @"http://localhost:8080" + SERVICE_PATH;
        public const string SERVICE_PATH = @"/quizservice";
    }
}