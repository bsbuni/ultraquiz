﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Ultra_Quiz.Models;

namespace Ultra_Quiz_Server
{
    public class QuizService : IQuizService
    {
        public List<int> GetQuestions(Category category, Level level)
        {
            throw new NotImplementedException();
        }

        public string GetQuestion(int id)
        {
            throw new NotImplementedException();
        }

        public bool CheckQuestion(int countdown, int id, string answer)
        {
            throw new NotImplementedException();
        }

        public ObservableCollection<User> GetHighscore()
        {
            throw new NotImplementedException();
        }

        public ObservableCollection<User> GetUsers()
        {
            throw new NotImplementedException();
        }

        public ObservableCollection<Category> GetCategories()
        {
            throw new NotImplementedException();
        }

        public ObservableCollection<Level> GetLevels()
        {
            throw new NotImplementedException();
        }
    }
}
