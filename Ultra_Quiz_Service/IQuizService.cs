﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Ultra_Quiz.Models;

namespace Ultra_Quiz_Service
{
 [ServiceContract]
    public interface IQuizService
    {
        [OperationContract]  
        List<int> GetQuestions(Category category, Level level);

        [OperationContract]  
        string GetQuestion(int id);

        [OperationContract]  
        bool CheckQuestion(int countdown, int id, string answer);

        [OperationContract]  
        ObservableCollection<User> GetHighscore();

        [OperationContract]  
        ObservableCollection<User> GetUsers();
        
        [OperationContract]   
        ObservableCollection<Category> GetCategories();
        
        [OperationContract]
        ObservableCollection<Level> GetLevels();
    }
}
