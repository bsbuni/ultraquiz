﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Ultra_Quiz.Core;
using Ultra_Quiz.Models;

namespace Ultra_Quiz_Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class QuizService : IQuizService
    {
        public List<int> GetQuestions(Category category, Level level)
        {
            return new PlayValidationProvider().GetQuestions(category,level);
        }

        public string GetQuestion(int id)
        {
            throw new NotImplementedException();
        }

        public bool CheckQuestion(int countdown, int id, string answer)
        {
            throw new NotImplementedException();
        }

        public ObservableCollection<User> GetHighscore()
        {
            throw new NotImplementedException();
        }

        public ObservableCollection<User> GetUsers()
        {
            throw new NotImplementedException();
        }

        public ObservableCollection<Category> GetCategories()
        {
            throw new NotImplementedException();
        }

        public ObservableCollection<Level> GetLevels()
        {
            throw new NotImplementedException();
        }

        public bool IsConnected()
        {
            return true;
        }
    }
}
